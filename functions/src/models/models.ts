import * as firebase from "firebase";

export interface School extends Heritage {
    schoolId: string;
    schoolName: string;
    schoolNumber: string;
    schoolEmail: string;
    profileURL?: Storage;
    joinURL?: string;
    token?: string;
    billingActive?: Billing;
}

export interface Billing extends Heritage {
    docId?: string
    typeAccount: TypeAccount;
    textCount: number;
    mediaCount: number;
    isActive: boolean;
    expiredAt: firebase.firestore.Timestamp;
}

export enum TypeAccount {
    PROFESSIONAL = "professional",
    BUSINESS = "business",
    ENTERPRISE = "enterprise"
}

export interface Formula {
    textCount: number
    mediaCount: number
    price: number
}

export interface Message extends Heritage {
    messageId?: string;
    messageBody: string;
    destinataires: Member[];
    schoolId?: string;
    classrooms: string[];
}

export interface Deliver {
    id: string;
    number: string;
    user?: string;
    sentAt?: string;
    processedAt?: string,
    deliverAt?: string,
}

export interface ClassRoom extends Heritage {
    id?: string;
    name: string;
    schoolId: string;
}

export interface Member extends Heritage {
    id?: string;
    name: string;
    lastName: string;
    classrooms?: userClassrooms[];
    phone?: Phone;
    schoolId: string;
}

export interface Phone {
    dialCode: string;
    number: string;
    internationalNumber: string;
    countryCode: string;
    nationalNumber: string;
    e164Number: string;
}

export interface userClassrooms {
    classroomId: string
    name: string;
    joinedAt: string
}

interface Heritage {
    createdAt?: firebase.firestore.Timestamp;
    updatedAt?: firebase.firestore.Timestamp;
}

interface Storage {
    downloadURL: string,
    filePath: string
}