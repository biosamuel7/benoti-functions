import { firestore, pubsub } from "firebase-functions";
// import { map, mergeMap, take, tap, toArray } from 'rxjs/operators';

import * as admin from "firebase-admin";
import { Member } from "../models/models";
const db = admin.firestore()

export const add = firestore
.document('members/{docId}')
.onCreate( async(snapshot, context) => {
    const member = snapshot.data() as Member
    const schoolId = member.schoolId as string
    console.log('member =>',member)

    const snapSchool = await db.collection('schools').doc(schoolId).get()

    if(!snapSchool.exists) return

    const school = snapSchool.data()

    const welcomeMsg = `Bienvenue {{LASTNAME}} {{NAME}} au groupe tu recevras des informations de ce numero`
    .replace("{{NAME}}",member.name)
    .replace("{{LASTNAME}}",member.lastName)

    console.log('school  =>', school)
    console.log('message  =>', welcomeMsg)

    return null
})

export const del = firestore
.document('members/{docId}')
.onDelete( async(snapshot, context) => {
    const data = snapshot.data() as Member
    console.log('delete =>',data)

    return null
})


export const birthDay = pubsub.schedule('00 09 *  *  *')
.onRun((context) => {
    console.log('chaque 09h du matin')
    console.log(context.timestamp)
})