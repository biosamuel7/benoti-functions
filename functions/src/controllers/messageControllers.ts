import * as functions from 'firebase-functions';
import { from, throwError } from 'rxjs';
import * as request from "request";
import { catchError, map, mergeMap, take, toArray } from 'rxjs/operators';
// import { XMLHttpRequest } from 'xmlhttprequest';

import * as admin from "firebase-admin";
import { Billing, Member, Message, School } from '../models/models';

const db = admin.firestore()

export const add = functions
.firestore
.document('messages/{docId}')
.onCreate( async(snapshot, context) => {
    const docId = context.params.docId
    const data = snapshot.data() as Message
    const messageBody = data.messageBody
    const destinataire = data.destinataires as Member[]
    const schoolId = data.schoolId as string

    console.log('docId =>',docId)

    const schoolCollection = db.collection('schools').doc(schoolId)

    const snapSchool = await schoolCollection.get()

    if(!snapSchool.exists) return

    const school = snapSchool.data() as School

    console.log('destinaires  =>', destinataire)
    console.log('school  =>', school)

    return from(destinataire)
    .pipe(mergeMap( async({name, lastName, phone}) => {

        const message = messageBody
        .replace("{{NAME}}",name)
        .replace("{{LASTNAME}}",lastName)

        console.log('message =>',message)
        console.log('phone =>',phone)

        var options = { method: 'POST',
        url: 'https://api.wassi.chat/v1/messages',
        headers:
         { token: school.token,
           'content-type': 'application/json' 
          },
        body: 
         { phone: phone?.e164Number,
           message: message
          },
        json: true 
    };

        return request(options,(res) => {
            console.log('res',res)
        })

        // return interval(500).pipe(map(() => ({id: docId, number: number, user: name} as Deliver)))
    }))
    .pipe(
        catchError(err => {
            console.error('err wa send', err)
            return throwError(err)
        }),
        take(destinataire.length), toArray()
    // ,tap(res => console.log('res',res))
    )
    .pipe(map(async result => {
        const billingCollection = schoolCollection.collection('billings')
        const docBilling = await billingCollection.where('isActive','==',true).limit(1).get()
        let billings: Billing[] = []
        if(!docBilling.empty){
            docBilling.forEach(elt => {
                billings.push({...elt.data(), docId: elt.id} as Billing)
            })
        }
        console.log('billings => ',billings)
        const p1 = billingCollection.doc(billings[0].docId as string).update({
            textCount: admin.firestore.FieldValue.increment(result.length)
        })
        
        const p2 = db.collection('messages').doc(docId).set({
            delivers: result
        },{merge: true})

        return Promise.all([p1,p2])
    }))
    .pipe(map(res => console.log('result =>',res)))
    .toPromise()
})