import * as admin from "firebase-admin";

admin.initializeApp()


import * as messageCtr from  "./controllers/messageControllers";
export const MESSAGE_ADD = messageCtr.add


import * as memberCtr from "./controllers/memberControllers";
export const MEMBER_ADD = memberCtr.add
export const BIRTHDAY = memberCtr.birthDay


import * as express from "express";
import * as cors from "cors";
import * as functions from 'firebase-functions';

const app = express();

app.use(cors({ origin: true }));

app.get('/',(req, res) => {
    console.log(req.body)
    res.status(200).send('coooool')
})

app.post('/',(req, res) => {
    console.log(req.body)
    res.status(200).send('well done')
})

export const webhook = functions.https.onRequest(app)